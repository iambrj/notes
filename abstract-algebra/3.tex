\documentclass[titlepage, 12pt]{article}
\usepackage[parfill]{parskip}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{xcolor}
\usepackage{setspace}
\usepackage{hyperref}
\usepackage{tcolorbox}
\usepackage{epigraph}

\tcbuselibrary{theorems}

\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=blue,
}

\newtcbtheorem[]{definition}{Definition}%
{colback=magenta!5,colframe=magenta!100!black,fonttitle=\bfseries}{th}

\newtcbtheorem[]{proposition}{Proposition}%
{colback=cyan!5,colframe=cyan!100!black,fonttitle=\bfseries}{th}

\newtcbtheorem[]{theorem}{Theorem}%
{colback=orange!5,colframe=orange!100!black,fonttitle=\bfseries}{th}

\begin{document}
\begin{titlepage} % Suppresses headers and footers on the title page

	\raggedleft% Right align everything

	\vspace*{\baselineskip} % Whitespace at the top of the page

	{Bharathi Ramana Joshi\\\url{https://github.com/iambrj/notes}} % Author name

	\vspace*{0.167\textheight} % Whitespace before the title

	\textbf{\LARGE Notes on}\\[\baselineskip] % First title line

	\textbf{\textcolor{teal}{\huge Quotient Groups and Homomorphisms}}\\[\baselineskip] % Main title line which draws the focus of the reader

    {\Large \textit{Chapter 3 from Dummit \& Foote, $3^{rd}$ Ed.}} % Subtitle

	\vfill % Whitespace between the titles and the publisher

	\vspace*{3\baselineskip} % Whitespace at the bottom of the page

\end{titlepage}

\newpage

\begin{definition}{Kernel}{}
    If $\phi$ is a homomorphism $\phi:G\rightarrow H$, the \textbf{kernel} of
    $\phi$ is the set
    \begin{gather*}
        \{g\in G\mid\phi(g) = 1\}
    \end{gather*}
    and is denoted using ker$\phi$.
\end{definition}
If $G$ and $H$ are groups and $\phi:G\rightarrow H$ is a homomorphism
then
\begin{enumerate}
    \item$\phi(1_G) = 1_H$.
    \item$\phi(g^{-1}) = \phi(g)^{-1}$, $\forall g\in G$.
    \item$\phi(g^n) = \phi(g)^n$, $\forall n\in\mathbb{Z}$.
    \item ker$\phi$ is a subgroup of $G$.
    \item im$(\phi)$, the image of $G$ under $\phi$ is a subgroup of $H$.
\end{enumerate}

\begin{definition}{Quotient/Factor group}{}
    If $\phi:G\rightarrow H$ is a homomorphism with kernel $K$, the
    \textbf{quotient group} or \textbf{factor group}, $G/K$ (read $G$ modulo $K$
    or simply $G$ mod $K$) is the group whose elements are the fibers of $\phi$
    with group operation defined as follows: if $X$ is the fiber above $a$ and
    $Y$ is the fiber above $b$ then the product of $X$ with $Y$ is defined to be
    the fiber above the product $ab$.
\end{definition}
In other words, it is the group whose elements are subsets of $G$ being mapped
to the same element in $H$ and group operation is as defined above.

\begin{proposition}{}{}
    If $\phi:G\rightarrow H$ is a homomorphism with kernel $K$ and $X\in G/K$
    such that $X = \phi^{-1}(a)$ for some $a$ then
    \begin{gather*}
        \forall u\in X,\;X = \{uk | k\in K\} = \{ku | k\in K\}.
    \end{gather*}
\end{proposition}
\begin{definition}{Cosets}{}
    $\forall N\le G$ and $\forall g\in G$
    \begin{gather*}
       gN = \{gn | n\in N\}\\
       Ng = \{ng | n\in N\}
    \end{gather*}
    are defined as the \textbf{left coset} and \textbf{right coset} of $N$ in
    $G$ respectively. Any element of a coset is called a \textbf{representative}
    for the coset.
\end{definition}
\begin{theorem}{}{}
    If $G$ is a group and $K$ is the kernel of some homomorphism from $G$ to
    another group, then the set whose elements are the left cosets of $K$ in $G$
    with the operation defined by
    \begin{gather*}
        uK\circ vK = (uv)K
    \end{gather*}
    forms the group $G/K$.
\end{theorem}
\begin{proposition}{}{}
    If $N$ is any subgroup of the group $G$, the set of left (right) cosets of $N$ in
    $G$ form a partition of $G$. Furthermore, $\forall u, v\in G, uN = vN$ iff
    $v^{-1}u\in N$ and in particular, $uN = vN$ iff $u$ and $v$ are
    representatives of the same coset.
\end{proposition}
\textbf{Proof sketch:} $1\in N$ therefore $\bigcup\limits_{g\in G} gN = G$ and
$uN\cap vN = \phi$ for $u$ and $v$ from different cosets since otherwise
\begin{align*}
    un_1 &= vn_2\\
    \iff u(n_1n_2^{-1}) &= v\implies v\in uN\;\textrm(contradiction!)
\end{align*}

\begin{proposition}{}{}
    If $G$ is a group, then $\forall N\le G$,
    \begin{enumerate}
        \item The operation on the set of left (right) cosets of $N$ in $G$ defined as
            \begin{gather*}
                uN\cdot vN = (uv)N
            \end{gather*}
            is well defined iff $gng^{-1}\in N,\;\forall g\in G$ and $\forall
            n\in N$.
        \item If the above operation is well defined, then the set of left (right)
            cosets of $N$ in $G$ form a group whose identity is $1N$ and
            $(gN)^{-1} = g^{-1}N$. This group is denoted by $G/N$.
    \end{enumerate}
\end{proposition}
\textbf{Proof sketch:} For the forward proof, we want to prove $gNg^{-1} = N$
from $uN.vN=(uv)N$. Multiplying both sides by $N$,
\begin{align*}
    &gNg^{-1} = N\\
    \iff &gN^{-1}N = N\\
    \iff &N = N
\end{align*}
For the backward proof we just show $uN.vN = (uv)N$ from $gN = Ng$.

Corollary: a subgroup $N$ of $G$ is normal iff it is the kernel of some
homomorphism.

\begin{definition}{Conjugate}{}
    The element $gng^{-1}$ is called the \textbf{conjugate} of $n\in N$ by $g$
    and the set $gNg^{-1} = \{gng^{-1}\;|\;n\in N\}$ is called the
    \textbf{conjugate} of $N$ by $g$. An element $g\in G$ is said to
    \textbf{normalize} $N$ if $gNg^{-1} = N$. A subgroup $N$ of $G$ is called
    \textbf{normal} if $gNg^{-1} = N,\;\forall g\in G$. If $N$ is a normal
    subgroup of $G$, it is written as $N\trianglelefteq G$.
\end{definition}
\begin{theorem}{}{}
    $\forall N\le G$, the following are equivalent
    \begin{enumerate}
        \item $N\trianglelefteq G$.
        \item $N_G(N) = G$.
        \item $gN = Ng,\;\forall g\in G$.
        \item The operation on left (right) cosets defined above makes it a
            group.
        \item $gNg^{-1}\subseteq N,\;\forall g\in G$.
        \item $N$ is the kernel of some homomorphism from $G$
    \end{enumerate}
\end{theorem}
Tips to minimize computations necessary to determine whether a subgroup $N$ of
$G$ is normal
\begin{enumerate}
    \item Avoid as much as possible the computation of all the conjugates
        $gng^{-1}$, $\forall n\in N$ and $\forall g\in G$.
    \item If set of generators of $N$ are known, it suffices to check that all
        conjugates of these generators lie in $N$ to prove $N$ is normal.
    \item If set of generators of $G$ are known, it suffices to check that these
        generators normalize $N$.
    \item If generators for both $N$ and $G$ are known, and $N$ is finite, it
        suffices to check that the conjugates of a set of generators for $N$ by
        a set of generators for $G$ are again elements of $N$.
\end{enumerate}
\begin{definition}{Natural projection/homomorphism}{}
    If $N\trianglelefteq G$ then then homomorphism defined by $\pi:G\rightarrow
    G/N$ defined by $\pi(g) = gN$ is called the \textbf{natural
    projection/homomorphism} of $G$ onto $G/N$. If $\overline H\le G/N$ is a
    subgroup of $G/N$, the \textbf{complete preimage} of $\overline H$ in $G$ is
    the preimage of $\overline H$ under the natural projection homomorphism.
\end{definition}
\begin{theorem}{Lagrange's Theorem}{}
    If $G$ is a finite group and $H$ is a subgroup of $G$, then the order of $H$
    divides the order of $G$ and the number of left cosets of $H$ in $G$ is
    $|G|/|H|$.
\end{theorem}
\textbf{Proof sketch:} By Proposition 2, the left (right) cosets of $H$ form a
partition of $G$. Furthermore, $\forall g\in G$, $|gH| = |H|$ since otherwise
$gh_1$ = $gh_2$ (for distinct $h1, h2\in H$) $\implies h_1 = h_2$.
\begin{definition}{Index}{}
    If $G$ is a group and $H\le G$, the number of left cosets of $H$ in $G$ is
    called the \textbf{index} $H$ in $G$ and is denoted by $|G:H|$.
\end{definition}
A few corollaries that follow
\begin{enumerate}
    \item If $G$ is a finite group, then $\forall x\in G$ $|x|\mid |G|$ and in
        particular $x^{|G|} = 1$.
    \item If $G$ is a group of prime order $p$, then $G$ is cyclic (and $G\cong
        Z_p$)
\end{enumerate}
\begin{theorem}{Cauchy's Theorem}{}
    If $G$ is a finite group and $p$ is a prime dividing $|G|$, then $G$ has an
    element of order $p$.
\end{theorem}
\textbf{Proof sketch}: case wise induction on $G$
\begin{theorem}{Sylow's Theorem}{}
    If $G$ is a finite group of order $p^\alpha m$ where $p$ is a prime and
    $p\nmid m$, then $G$ has a subgroup of order $p^\alpha$.
\end{theorem}
\begin{proposition}{}{}
    If $H$ and $K$ are finite subgroups of a group then,
    \begin{gather*}
        |HK| = \frac{|H||K|}{|H\cap K|}
    \end{gather*}
\end{proposition}{}{}
\textbf{Proof sketch}: $|HK|$ = number of distinct left cosets of $H$ in $K$
$\times$ size of each coset = $\frac{|H|}{|H\cap K|}\times |K|$
\begin{proposition}{}{}
    If $H$ and $K$ are subgroups of a group, $HK$ is a subgroup iff $HK = KH$.
\end{proposition}
\textbf{Proof sketch}:  For the forward implication, show that $HK\subseteq KH$
and $KH\subseteq HK$. For instance for arbitrary $h_1k_1\in HK$, $\exists
h_2k_2\in HK$ such that $h_1k_1 = (h_2k_2)^{-1} = k_2^{-1}h_2^{-1}\in KH$. For
the backward implication, observe that for some $a = h_1k_1$ and $b = h_2k_2$ in
$HK$, it suffices to show $ab^{-1} = h_1k_1k_2^{-1}h_2^{-1}\in HK$.

An interesting corollary is that if $ K \le H \le G$ and
$H\le N_G(K)$, then $HK$ is a subgroup of $G$ and in particular if
$K\trianglelefteq G$ then $HK\le G$, $\forall H\le G$.
\begin{definition}{}{}
    If $A$ is a subset of $N_G(K)$ (or $C_G(K)$), A is said to
    \textbf{normalize} (\textbf{centralize}) K.
\end{definition}
\begin{theorem}{First Isomorphism Theorem/\\Fundamental Theorem of Homomorphisms}{}
    If $\phi:G\rightarrow H$ is a homomorphism, then ker$\phi\trianglelefteq G$
    und $G/\text{ker}\phi\cong \phi(G)$.
\end{theorem}
\begin{theorem}{Second Isomorphism Theorem/\\Diamond Isomorphism Theorem}{}
    If $G$ is a group, $A$ and $B$ are subgroups of $G$ and $A\le N_G(B)$, then
    $AB$ is a subgroup of $G$, $B\trianglelefteq AB$, $A\cap B\trianglelefteq A$
    and $AB/B\cong A/A\cap B$.
\end{theorem}
Intuition: We have a subgroup $S$ and a normal subgroup $N$ of $G$. Now, we want
to quotient out $N$ from $S$ - however, we do not know if $N$ is also a subgroup
of $S$. Thus, there are two options:
\begin{enumerate}
    \item Quotient out $N$ from the smallest subgroup of $G$ containing both
        $S$\&$N$
    \item Quotient out the intersection from $S$
\end{enumerate}
\begin{theorem}{Third Isomorphism Theorem}{}
    If $G$ is a group, $A$ and $B$ are normal subgroups of $G$ such that $A\le
    B$, then $B/A\trianglelefteq G/A$ and 
    \begin{align*}
        (G/A)/(B/A) \cong G/B
    \end{align*}.
\end{theorem}
\begin{theorem}{Fourth Isomorphism Theorem/\\Lattice Isomorphism Theorem}{}
    If $G$ is a group and $N$ is a normal subgroup of $G$, then there is a
    bijection from the set of subgroups $A$ of $G$ which contain $N$ onto the
    set of subgroups $\overline A = A/N$ of $G/N$. In particular, every subgroup
    of $\overline G$ is of the form $A/N$ for some subgroup $A$ of $G$
    containing $N$. This bijection has the following properties, $\forall$
    subgroups $A, B$ containing $N$
    \begin{enumerate}
        \item $A\le B \iff \overline A\le\overline B$
        \item $A\le B\implies |B:A| = |\overline B:\overline A|$
        \item $\overline{\langle A, B\rangle} = \langle\overline A, \overline
            B\rangle$
        \item $\overline{A\cup B} = \overline A\cup\overline B$
        \item $A\trianglelefteq G$ iff $\overline A\trianglelefteq\overline G$
    \end{enumerate}
\end{theorem}
\begin{proposition}{}{}
    If $G$ is a finite abelian group and $p$ is a prime dividing $|G|$, then $G$
    contains an element of order $p$.
\end{proposition}
\textbf{Proof}: By induction on $G$
\begin{definition}{Simple group}{}
    A group $G$ is called \textbf{simple} if $|G|\ge 1$ and the only normal
    subgroups of $G$ are 1 and $G$.
\end{definition}
\begin{definition}{Composition series, factors}{}
    In a group $G$ a sequence of subgroups
    \begin{gather*}
        1 = N_0\le N_1\le\dots\le N_{k-1}\le N_k = G
    \end{gather*}
    is called a \textbf{composition series} if $N_i\trianglelefteq N_{i+1}$ and
    $N_{i+1}/N_i$ is a simple group, $0\le i\le k-1$ and the quotient groups
    $N_{i+1}/N_i$ are called \textbf{composition factors} of $G$.
\end{definition}
\begin{theorem}{Jordan-H\"older theorem}{}
    If $G$ is a finite group with $G\neq 1$ then
    \begin{enumerate}
        \item $G$ has a composition series
        \item The composition factors in a composition series are unique,
            namely, if $1 = N_0\le N_1\le\dots\le N_{r-1}\le N_r = G$ and $1 =
            M_0\le M_1\le\dots\le M_{s-1}\le M_s = G$ are two composition series
            for $G$, then $r = s$ and $\exists$ permutation $\pi$ of
            $\{1,\dots,n\}$ such that
            \begin{gather*}
                M_{\pi[(i)]}/M_{\pi[(i)] - 1}\cong N_i/N_{i - 1},\; 1\le i\le r
            \end{gather*}
    \end{enumerate}
\end{theorem}
\begin{center}
    \large{\textbf{The H\"older Program}}
    \begin{enumerate}
        \item Classify all finite simple groups.
        \item Find all ways of ``putting simple groups together'' to form other
            groups.
    \end{enumerate}
\end{center}
\begin{theorem}{Feit-Thompson theorem}{}
    If $G$ is a simple group of odd order, then $G\cong Z_p$ for some prime
    $p$.
\end{theorem}
\begin{quotation}
This proof takes 255 pages of hard mathematics
\end{quotation}
\begin{definition}{Solvable group}{}
    A group $G$ is \textbf{solvable} if $\exists$ a chain of subgroups
    \begin{gather*}
        1 = G_0\trianglelefteq G_1\dots\trianglelefteq G_s = G
    \end{gather*}
    such that $G_{i+1}/G_i$ is abelian $\forall i\in[0, s - 1]$
\end{definition}
\begin{theorem}{}{}
    A finite group $G$ is solvable iff for every divisor $n$ of $|G|$ such that
    $(n, \frac{|G|}{n}) = 1$, $G$ has a subgroup of order $n$.
\end{theorem}
\begin{proposition}{}{}
    If $N\trianglelefteq G$ such that $N$ and $G/N$ are solvable, the $G$ is
    solvable.
\end{proposition}
\begin{definition}{Transportation}{}
    A 2-cycle is called a \textbf{transportation}.
\end{definition}
\begin{definition}{Sign, Even/Odd permutation}{}
    \begin{enumerate}
        \item $\epsilon(\sigma)$ is called the \textbf{sign} of $\sigma$
        \item $\sigma$ is called an \textbf{even permutation} if
            $\epsilon(\sigma) = 1$ and \textbf{odd permutation} if
            $\epsilon(\sigma) = -1$.
    \end{enumerate}
\end{definition}
\begin{proposition}{}{}
    The map $\epsilon:S_n\rightarrow\{\pm 1\}$ is a homomorphism.
\end{proposition}
\begin{proposition}{}{}
    Transportations are all odd permutations and $\epsilon$ is a surjective
    homomorphism.
\end{proposition}
\begin{definition}{Alternating group}{}
    The \textbf{alternating group of degree $n$}, denoted by $A_n$, is the
    kernel of the homomorphism $\epsilon$
\end{definition}
\begin{proposition}{}{}
    The permutation $\sigma$ is odd iff the number of cycles of even length in
    its decomposition is odd
\end{proposition}
\begin{definition}{}{}
    For any group $G$,
    \begin{gather*}
        N = \langle x^{-1}y^{-1}xy | x, y\in G\rangle
    \end{gather*}
    is called the \textbf{commutator subgroup} of $G$.
\end{definition}
\end{document}
